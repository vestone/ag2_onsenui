import { Component } from '@angular/core';
import { OnsNavigator } from 'ngx-onsenui';
import { FirstPageComponent } from './first.page.component';

@Component({
	selector: 'ons-page[second]',
	templateUrl: './second.page.component.html'
})
export class SecondPageComponent {
	constructor(private navigator: OnsNavigator) {}
	pop() {
		//this.navigator.element.pushPage(FirstPageComponent);
		this.navigator.element.popPage();
	}
}