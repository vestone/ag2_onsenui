import { Component } from '@angular/core';
import { OnsNavigator } from 'ngx-onsenui';
import { SecondPageComponent } from './second.page.component';
import * as ons from 'onsenui';

@Component({
  selector: 'ons-page[first]',
  templateUrl: './first.page.component.html',
  styleUrls: ['first.page.component.css']
})
export class FirstPageComponent {
	constructor (private navigator: OnsNavigator) {}
	add() {
		//this.navigator.element.popPage();
		//this.navigator.element.pushPage(SecondPageComponent);
		ons.notification.alert('Is in progress');

	}
}

