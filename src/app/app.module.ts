import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FirstPageComponent } from './first.page.component';
import { SecondPageComponent } from './second.page.component';

/* onsenui imports */
import { OnsenModule } from 'ngx-onsenui';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

@NgModule({
  declarations: [
    AppComponent,
	  FirstPageComponent,
    SecondPageComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
    ],
  imports: [
    BrowserModule,
    OnsenModule
  ],
  providers: [],
  entryComponents: [
    FirstPageComponent,
    SecondPageComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
